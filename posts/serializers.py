from rest_framework import serializers

from posts.models import Rate
from .models import Post, Media, Tag, Rate
from .utils import extract_hashtags


class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Media

        fields = ['id', 'post', 'media_type', 'media_url', 'media_server_path', 'media_file_name',
                  'media_file_extension', 'created_at', 'modified_at']

    def create(self, validated_data):
        return super(MediaSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        post = super(MediaSerializer, self).update(instance, validated_data)
        try:
            post.save()
        except Exception as ex:
            pass

        return post


class PostSerializer(serializers.ModelSerializer):
    media = serializers.ListField(read_only=True)
    mentions = serializers.ListField(read_only=True)
    owner = serializers.ReadOnlyField()
    category = serializers.ReadOnlyField()
    rate = serializers.IntegerField(read_only=True, default=0)
    rate_count = serializers.IntegerField(read_only=True, default=0)
    rate_average = serializers.FloatField(read_only=True, default=0)

    class Meta:
        model = Post

        fields = ['id', 'caption', 'category', 'hashtag',
                  'post_owner', 'media',
                  'mentions', 'owner', 'rate', 'rate_count', 'rate_average']
        extra_kwargs = {
            'post_owner': {'write_only': True},
            'category': {'write_only': True},
        }

    def create(self, validated_data):
        new_post_object = super(PostSerializer, self).create(validated_data)

        post_caption = validated_data['caption']
        extracted_hashtags = extract_hashtags(post_caption)

        for hashtag in extracted_hashtags:
            selected_tag, is_created = Tag.objects.get_or_create(title=hashtag)
            new_post_object.hashtag.add(selected_tag)

        new_post_object.save()
        return new_post_object

    def update(self, instance, validated_data):
        post = super(PostSerializer, self).update(instance, validated_data)
        try:
            post.hashtag.clear()
            post_caption = validated_data['caption']
            extracted_hashtags = extract_hashtags(post_caption)

            for hashtag in extracted_hashtags:
                selected_tag, is_created = Tag.objects.get_or_create(title=hashtag)
                post.hashtag.add(selected_tag)

            post.save()

        except Exception as ex:
            pass

        return post

    def to_representation(self, instance):
        media_list = Media.objects.filter(post_id=instance.id) \
            .values('id', 'media_url', 'media_file_name', 'media_file_extension')

        instance.owner = {
            'id': instance.post_owner.id,
            'username': instance.post_owner.username,
            'avatar': instance.post_owner.avatar,

        }
        instance.category = {
            'id': instance.post_category.id,
            'category': instance.post_category.category,

        }
        my_rate = Rate.objects.filter(post=instance.id, user=self.context['request'].user).first()
        if my_rate is not None:
            instance.rate = my_rate.rate

        instance.rate_count = Rate.objects.filter(post=instance.id).count()
        if instance.rate_count > 0:
            instance.rate_average = sum([float(x["rate"]) for x in
                                Rate.objects.filter(post=instance.id).values("rate")])/ instance.rate_count

        instance.media = media_list

        # instance.mentions = instance.post_mentions.all().values('id', 'username')
        data = super(PostSerializer, self).to_representation(instance)
        return data


class RateSerializer(serializers.ModelSerializer):
    user_like = serializers.CharField(max_length=200, required=False)

    class Meta:
        model = Rate
        fields = ['id', 'user', 'post', 'user_like']

    def create(self, validated_data):
        return super(RateSerializer, self).create(validated_data)

    def to_representation(self, instance):
        user_like = instance.user.username
        instance.user_like = user_like
        data = super(RateSerializer, self).to_representation(instance)
        return data
