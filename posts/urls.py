
from django.urls import path
from posts.views import PostsCreateAndList, RateListOrCreate, UpdatePost, FileUploadView

urlpatterns = [
    path('', PostsCreateAndList.as_view(), name='fileUpload'),
    path('<str:pk>/like', RateListOrCreate.as_view(), name='RateList'),
    path('upload-file/', FileUploadView.as_view(), name='fileUpload'),
    path('<str:pk>/', UpdatePost.as_view(), name='UpdatePostList'),
]