from django.contrib import admin
from .models import Post, Category, Tag


class PostsAdmin(admin.ModelAdmin):
    pass

admin.site.register(Post)
admin.site.register(Tag)
admin.site.register(Category)
