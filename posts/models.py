import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _
from accounts.models import Account


class Tag(models.Model):
    class Meta:
        db_table = "post_tags"

    title = models.CharField(max_length=100, primary_key=True)
    use_count = models.IntegerField(default=1)


class Category(models.Model):
    class Meta:
        db_table = "categories"

    id = models.UUIDField(default=uuid.uuid4, blank=False, primary_key=True)
    category = models.CharField(max_length=100)


class Post(models.Model):

    class PostStatus(models.TextChoices):
        WAITING = 'WI', _('Waiting')
        ACCEPTED = 'AC', _('Accepted')
        DELETED = 'DL', _('Delete')

    class Meta:
        db_table = "posts"

    id = models.UUIDField(default=uuid.uuid4, blank=False, primary_key=True)
    caption = models.CharField(max_length=200, blank=False)
    body = models.TextField(blank=False)
    status = models.CharField(max_length=2, choices=PostStatus.choices, default=PostStatus.WAITING)
    hashtag = models.ManyToManyField(Tag, blank=True)
    post_owner = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='post_owner')
    post_category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='post_category')


class Media(models.Model):
    class MediaType(models.TextChoices):
        KNOWN = 'KN', _('Known')
        VIDEO = 'VI', _('Video')
        IMAGE = 'IM', _('Image')
        SOUND = 'SO', _('Sound')

    id = models.UUIDField(default=uuid.uuid4, blank=False, primary_key=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    media_type = models.CharField(max_length=2, choices=MediaType.choices, default=MediaType.IMAGE)
    media_url = models.CharField(max_length=255)
    media_server_path = models.CharField(max_length=255)
    media_file_name = models.CharField(max_length=255)
    media_file_extension = models.CharField(max_length=10)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "post_medias"


class Rate(models.Model):
    id = models.UUIDField(default=uuid.uuid4, blank=False, primary_key=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=False)
    user = models.ForeignKey(Account, on_delete=models.CASCADE, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    rate = models.IntegerField(default=1, null=False)

    class Meta:
        db_table = "post_rate"
