# DRF Test SnappFood Application
Sample account and post platform

# Introduction
In this platform you can create user and login with user 
after login you can create post with caption and text and tag and category.
after you create post you and other user can rate this.
rate must be between 0 and 5 and user can't remove rate.
and in list of post you can see count of rate and average of rate and 
if you rate this , show your rate in output.


# Requirements
Simply run the following snippet code in your shell.

```pip install -r requirements.txt```

# How to run the project
After installing the requirements of the project, run the following code in your cloned directory to start the project.

```python manage.py runserver```


# How to run the project with docker-compose
Open docker service and run this :
```docker-composee up -d```